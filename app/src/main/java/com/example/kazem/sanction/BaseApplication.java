package com.example.kazem.sanction;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
    }
}
