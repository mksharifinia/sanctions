package com.example.kazem.sanction.BroadCastReciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.TimedText;
import android.preference.PreferenceManager;
import android.support.v4.util.TimeUtils;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class BroadCastReciver extends BroadcastReceiver {


    @Override
    public void onReceive(final Context context, Intent intent) {
        TelephonyManager telephony = (TelephonyManager)context
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(new PhoneStateListener(){
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);

                Hawk.put("incomingNumber",incomingNumber);
                Toast.makeText( context,incomingNumber, Toast.LENGTH_SHORT).show();
            }
        },PhoneStateListener.LISTEN_CALL_STATE);

    }
}
