package com.example.kazem.sanction.Other;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kazem.sanction.R;

public class KrimActivity extends Activity implements View.OnClickListener {
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_krim);
        editText=findViewById(R.id.etxt);
        findViewById(R.id.btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.btn){
            Toast.makeText(this,editText.getText()+" "+"بوخدا مسلمون نیستی",Toast.LENGTH_LONG).show();
        }
    }
}
