package com.example.kazem.sanction.cosin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.BackgroundColorSpan;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.kazem.sanction.R;
import com.nikitagordia.cosin.Cosin;
import com.nikitagordia.cosin.textAdapters.WordTextAdapter;

public class CosinActivity extends AppCompatActivity {

    Cosin cosin;
    TextView textView;
    SeekBar seekBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosin);
        textView=findViewById(R.id.textcosin);
        cosin=findViewById(R.id.Cosin);
        Cosin.TextAdapter textAdapter=new WordTextAdapter("Kazem");
        cosin.setTextAdapter(textAdapter);
        cosin.setBackgroundColor(2);
        seekBar=findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                cosin.setPeriod((double)i/10);
                textView.setText(cosin.getPeriod()+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }






}
