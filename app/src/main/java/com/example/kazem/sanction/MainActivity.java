package com.example.kazem.sanction;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView Email,Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();


    };

    void bind(){
        
        Email=findViewById(R.id.Email);
        Password=findViewById(R.id.Password);
        findViewById(R.id.SignUp).setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.SignUp && android.util.Patterns.EMAIL_ADDRESS.matcher(Email.getText().toString()).matches())
        {
            showSignUpToast();

        }else if (view.getId()==R.id.SignUp) {
            showNotValid();
        }
    }
    void showSignUpToast(){
        Toast.makeText(MainActivity.this,Email.getText().toString()+
                getString(R.string.sanctionstring),Toast.LENGTH_LONG).show();
    }
    void showNotValid(){
        Toast.makeText(MainActivity.this, R.string.EPisnotvalid,Toast.LENGTH_LONG).show();

    }
}
