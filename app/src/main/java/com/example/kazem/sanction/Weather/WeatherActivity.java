package com.example.kazem.sanction.Weather;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class WeatherActivity extends BasicActivity {

    EditText enterCity;
    TextView temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        bind();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://ip-api.com/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(mContext, throwable.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                parseAndSet(responseString);
            }
        });

    }

    void parseAndSet(String responseString) {
        try {

            JSONObject jSon = new JSONObject(responseString);
            String cityFromJson=jSon.getString("city").toLowerCase();
            enterCity.setText(cityFromJson);
            getWeatherFromWebService(cityFromJson);
        } catch (Exception e) {
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getWeatherFromWebService(String city) {

        String URL_WEB_SERVICE="https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                +city+"%2Cir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient clientWeather=new AsyncHttpClient();
        clientWeather.get(URL_WEB_SERVICE, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(WeatherActivity.this, "WSNA", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Toast.makeText(WeatherActivity.this, responseString, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void bind() {
        enterCity = findViewById(R.id.entercity);
        temp = findViewById(R.id.temp);

    }

}
