package com.example.kazem.sanction.SaveData;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.andreacioccarelli.cryptoprefs.CryptoPrefs;
import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;
import com.orhanobut.hawk.Hawk;

public class SharepreferenceActivity extends BasicActivity implements View.OnClickListener {

    TextView textView;
    EditText editText;
    CryptoPrefs prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = new CryptoPrefs(mContext, "CryptoFileName", "c29maWE=",true);
        setContentView(R.layout.activity_sharepreference);
        editText = findViewById(R.id.etxtshare);
        textView = findViewById(R.id.txtshare);
        findViewById(R.id.btnsave).setOnClickListener(this);
        findViewById(R.id.btnLoad).setOnClickListener(this);
        Hawk.init(mContext).build();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnsave) {
            saveData("email", editText.getText().toString());
            Toast.makeText(mContext, "data was saved", Toast.LENGTH_LONG).show();

        }
        if (view.getId() == R.id.btnLoad) {
            String username = loadData( "email","mksharifinia");
            textView.setText(username);
            Toast.makeText(mContext, username, Toast.LENGTH_LONG).show();
        }
    }

    public void saveData(String keyValue,String stringValue) {

//            prefs.put(keyValue,stringValue);
        Hawk.put(keyValue, stringValue);
//        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
//                .putString(keyValue, stringValue).apply();
    }

    public String loadData(String keyValue ,String defValue) {
        return Hawk.get(keyValue,defValue);

//        return PreferenceManager.getDefaultSharedPreferences(mContext)
//                .getString("name", "defaultValue[Mksharifinia]");

    }
}
