package com.example.kazem.sanction.intent;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;

import static java.security.AccessController.getContext;

public class OpenCameraActivity extends BasicActivity implements View.OnClickListener {
    Intent intent;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_camera);
         intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        findViewById(R.id.BtnOpenCamera).setOnClickListener(this);
        imageView=findViewById(R.id.imgview);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.BtnOpenCamera){
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                startActivityForResult(intent,130);
            }else {
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.CAMERA},130);
                startActivityForResult(intent,130);
            }


        }

        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK){
        if (requestCode==130){

            Bitmap image = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(image);

        }

        }
    }
}
