package com.example.kazem.sanction.intent;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;

public class CheckSensorActivity extends BasicActivity implements View.OnClickListener {

    TextView txtWifi, txtGps, txtBluetooth;
    WifiManager wifiManager;
    BluetoothAdapter mBluetoothAdapter;
    LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_sensor);
        bind();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        findViewById(R.id.wifi).setOnClickListener(this);
        findViewById(R.id.GPS).setOnClickListener(this);
        findViewById(R.id.Bt).setOnClickListener(this);
        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    void bind() {
        txtWifi = findViewById(R.id.txtwifi);
        txtGps = findViewById(R.id.txtGPS);
        txtBluetooth = findViewById(R.id.txtBt);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (wifiManager != null && wifiManager.isWifiEnabled()) {
            txtWifi.setText("Wifi is enable");


        } else {
            txtWifi.setText("Wifi is disable.");

        }

        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            txtBluetooth.setText("Bluetooth is enable.");
        } else {
            txtBluetooth.setText("Bluetooth is disable.");
        }

        if (mLocationManager != null && mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            txtGps.setText("GPS is enable.");
        } else {
            txtGps.setText("GPS is disable.");
        }


    }

    public void onClick(View view) {
        if (view.getId() == R.id.wifi) {
            checkPermissionWifi();
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));


        }
        if (view.getId() == R.id.GPS) {
            checkPermissionGps();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

        }
        if (view.getId() == R.id.Bt) {
            checkPermissionBlutooth();
            startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
        }
    }

    void checkPermissionWifi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {


            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_WIFI_STATE}, 100);  // Comment 26

            }
        }
    }

    void checkPermissionGps() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);  // Comment 26

            }
        }
    }

    void checkPermissionBlutooth() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED) {


            } else {
                requestPermissions(new String[]{Manifest.permission.BLUETOOTH}, 102);  // Comment 26

            }
        }

    }
}
