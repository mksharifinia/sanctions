package com.example.kazem.sanction.BroadCastReciver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;
import com.orhanobut.hawk.Hawk;

public class VideoViewActivity extends BasicActivity {
    TextView textCall;
    BroadcastReceiver receiver;
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bind();
        videoView.setVideoURI(Uri.parse("https://hw18.cdn.asset.aparat.com/aparat-video/6022b86d01ff381cf8bdf8c3afd468d911028535-144p__65181.mp4"));
        videoView.start();
        permisionPhoneState();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (videoView.isPlaying())
                    videoView.pause();
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(receiver, intentFilter);
       textCall.setText(Hawk.get("incomingNumber")+"");
        Toast.makeText(mContext, Hawk.get("incomingNumber")+"", Toast.LENGTH_SHORT).show();
    }

    void bind() {
        setContentView(R.layout.activity_video_view);
        textCall = findViewById(R.id.textCall);
        videoView = findViewById(R.id.videoView);
        videoView.setMediaController(new MediaController(mContext));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void permisionPhoneState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {


            } else {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 101);  // Comment 26

            }
        }
    }
}
