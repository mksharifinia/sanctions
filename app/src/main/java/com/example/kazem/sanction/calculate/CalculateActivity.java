package com.example.kazem.sanction.calculate;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.kazem.sanction.R;

public class CalculateActivity extends Activity implements View.OnClickListener {
    TextView textView;
    Double numberOne, otherNumber, result;
    String operation;
    Context mContect = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);
        bind();
        clickEvent();


    }

    void clickEvent() {
        findViewById(R.id.C).setOnClickListener(this);
        findViewById(R.id.negpos).setOnClickListener(this);
        findViewById(R.id.precent).setOnClickListener(this);
        findViewById(R.id.devision).setOnClickListener(this);
        findViewById(R.id.seven).setOnClickListener(this);
        findViewById(R.id.eight).setOnClickListener(this);
        findViewById(R.id.nine).setOnClickListener(this);
        findViewById(R.id.multiple).setOnClickListener(this);
        findViewById(R.id.four).setOnClickListener(this);
        findViewById(R.id.five).setOnClickListener(this);
        findViewById(R.id.six).setOnClickListener(this);
        findViewById(R.id.mines).setOnClickListener(this);
        findViewById(R.id.one).setOnClickListener(this);
        findViewById(R.id.two).setOnClickListener(this);
        findViewById(R.id.three).setOnClickListener(this);
        findViewById(R.id.plus).setOnClickListener(this);
        findViewById(R.id.zero).setOnClickListener(this);
        findViewById(R.id.point).setOnClickListener(this);
        findViewById(R.id.equal).setOnClickListener(this);
    }

    void bind() {
        textView = findViewById(R.id.textview);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.C:
                textView.setText("");
                numberOne = 0.0;
                otherNumber = 0.0;
                break;
            case R.id.negpos:

                if (textView.getText().length() > 0) {
                    if (textView.getText().toString().charAt(0) == '-') {
                        textView.setText(textView.getText().toString().replace("" +
                                "-", ""));
                    } else {
                        textView.setText("-" + textView.getText().toString());
                    }
                } else {
                    textView.setText("-" + textView.getText().toString());
                }
                break;
            case R.id.precent:
                break;
            case R.id.devision:
                numberOne = Double.parseDouble(textView.getText().toString());
                textView.setText("");
                operation = "/";
                break;
            case R.id.seven:
                textView.setText(textView.getText() + "7");
                break;
            case R.id.eight:
                textView.setText(textView.getText() + "8");
                break;
            case R.id.nine:
                textView.setText(textView.getText() + "9");
                break;
            case R.id.multiple:
                numberOne = Double.parseDouble(textView.getText().toString());
                textView.setText("");
                operation = "x";
                break;
            case R.id.four:
                textView.setText(textView.getText() + "4");
                break;
            case R.id.five:
                textView.setText(textView.getText() + "5");
                break;
            case R.id.six:
                textView.setText(textView.getText() + "6");
                break;
            case R.id.mines:
                numberOne = Double.parseDouble(textView.getText().toString());
                textView.setText("");
                operation = "-";
                break;
            case R.id.one:
                textView.setText(textView.getText() + "1");
                break;
            case R.id.two:
                textView.setText(textView.getText() + "2");
                break;
            case R.id.three:
                textView.setText(textView.getText() + "3");
                break;
            case R.id.plus:
                numberOne = Double.parseDouble(textView.getText().toString());
                textView.setText("");
                operation = "+";

                break;
            case R.id.zero:
                textView.setText(textView.getText() + "0");
                break;
            case R.id.point:
                if (textView.getText().length() > 0) {
                    if (!textView.getText().toString().contains("."))
                        textView.setText(textView.getText() + ".");
                } else {
                    textView.setText("0.");
                }
                break;
            case R.id.equal:


                try {
                    otherNumber = Double.parseDouble(textView.getText().toString());
                    switch (operation) {
                        case "+":
                            result = numberOne + otherNumber;
                            textView.setText(result.toString());
                            break;
                        case "-":
                            result = numberOne - otherNumber;
                            textView.setText(result.toString());
                            break;
                        case "x":
                            result = numberOne * otherNumber;
                            textView.setText(result.toString());
                            break;
                        case "/":
                            result = numberOne / otherNumber;
                            textView.setText(result.toString());
                            break;
                    }
                } catch (NumberFormatException e) {
                    textView.setText("Error");
                }


                break;
        }

    }
}



