package com.example.kazem.sanction.intent;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;

public class SecondIntentActivity extends BasicActivity implements View.OnClickListener {
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_intent);
        Intent intent=getIntent();

        textView=findViewById(R.id.txtViewSec);
        textView.setText(intent.getStringExtra("Number"));
        findViewById(R.id.btnvalid).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.btnvalid){
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result","Valid");
            setResult(Activity.RESULT_OK,returnIntent);
            finish();

        }
    }
}
