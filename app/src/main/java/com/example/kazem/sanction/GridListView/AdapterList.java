package com.example.kazem.sanction.GridListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.kazem.sanction.R;

import java.sql.Array;


public class AdapterList extends BaseAdapter {

    Context mContext;
    String[] list;

    public AdapterList(Context mContext, String[] list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.length ;
    }

    @Override
    public Object getItem(int position) {
        return list[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row= LayoutInflater.from(mContext)
                .inflate(R.layout.list_view_items,viewGroup,false);
        TextView textView=row.findViewById(R.id.textViewItemList);
        textView.setText(list[position]);

        return row;
    }
}
