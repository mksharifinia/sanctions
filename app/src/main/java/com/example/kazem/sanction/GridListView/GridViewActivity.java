package com.example.kazem.sanction.GridListView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends BasicActivity {
    List<ElectronicDevice> list;
    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);
        gridView=findViewById(R.id.gridViewActivity);
        ElectronicDevice electronicDevice_0 = new ElectronicDevice(
                "497-15101-1-ND", "$1.73000",
                "https://media.digikey.com/photos/STMicro%20Photos/MP34DT02TR.JPG"
        );
        ElectronicDevice electronicDevice_1 = new ElectronicDevice(
                "497-15703-1-ND", "$1.89000",
                "https://media.digikey.com/Renders/STMicro%20Renders/MP34DT04TR.jpg"
        );
        ElectronicDevice electronicDevice_2 = new ElectronicDevice(
                "497-14947-1-ND", "$$1.89000",
                "https://media.digikey.com/Photos/STMicro%20Photos/MP23AB02BTR.jpg"
        );
        ElectronicDevice electronicDevice_3 = new ElectronicDevice(
                "497-15102-2-ND", "$0.62500",
                "https://media.digikey.com/photos/STMicro%20Photos/MP34DT01TR-M.JPG"
        );
        list = new ArrayList<>();
        list.add(electronicDevice_0);
        list.add(electronicDevice_1);
        list.add(electronicDevice_2);
        list.add(electronicDevice_3);
        list.add(electronicDevice_1);
        list.add(electronicDevice_2);
        list.add(electronicDevice_3);
        list.add(electronicDevice_1);
        list.add(electronicDevice_2);
        list.add(electronicDevice_3);

        ElectronicAdapter electronicAdapter = new ElectronicAdapter(mContext, list);
        gridView.setAdapter(electronicAdapter);
    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            ElectronicDevice eD=(ElectronicDevice)
                    adapterView.getItemAtPosition(i);
            Toast.makeText(mContext, eD.getPartNumber(), Toast.LENGTH_SHORT).show();

        }
    });

    }
}
