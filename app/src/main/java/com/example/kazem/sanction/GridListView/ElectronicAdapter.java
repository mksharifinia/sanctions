package com.example.kazem.sanction.GridListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.kazem.sanction.R;

import java.util.List;

public class ElectronicAdapter extends BaseAdapter {

    Context mContext;
    List<ElectronicDevice> listElectronicDevice;


    public ElectronicAdapter(Context mContext, List<ElectronicDevice> listElectronicDevice) {
        this.mContext = mContext;
        this.listElectronicDevice = listElectronicDevice;
    }

    @Override
    public int getCount() {
        return listElectronicDevice.size();
    }

    @Override
    public Object getItem(int position) {
        return listElectronicDevice.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View elementView= LayoutInflater.from(mContext)
                .inflate(R.layout.list_grid_view,viewGroup,false);
        ImageView imageView=elementView.findViewById(R.id.imgview);
        TextView partNumber=elementView.findViewById(R.id.partNumber);
        TextView price=elementView.findViewById(R.id.price);

        Glide.with(mContext).
                load(listElectronicDevice.get(position).getIMAGE_URL())
                .into(imageView);
        partNumber.setText(listElectronicDevice.get(position).getPartNumber());
        price.setText(listElectronicDevice.get(position).getPrice());
        return elementView;
    }
}
