package com.example.kazem.sanction.intent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kazem.sanction.BasicActivity;
import com.example.kazem.sanction.R;

public class IntentActivity extends BasicActivity implements View.OnClickListener {
    Intent intent;
    EditText txtName, txtNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);
        bind();
        intent = new Intent(mActivity, SecondIntentActivity.class);
        findViewById(R.id.btnsend).setOnClickListener(this);

    }

    void bind() {
        txtName = findViewById(R.id.etname);
        txtNumber = findViewById(R.id.etnumber);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnsend) {
            intent.putExtra("Number",txtNumber.getText().toString());
            startActivityForResult(intent,150);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==150){
            if (resultCode== Activity.RESULT_OK){

                Toast.makeText(this, data.getStringExtra("result"), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
