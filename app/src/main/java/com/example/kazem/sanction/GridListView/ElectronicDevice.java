package com.example.kazem.sanction.GridListView;

public class ElectronicDevice {

    private String partNumber;
    private String Direction;
    private String price;
    private String IMAGE_URL;

    public ElectronicDevice(String partNumber, String price, String IMAGE_URL) {
        this.partNumber = partNumber;
        this.price = price;
        this.IMAGE_URL = IMAGE_URL;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIMAGE_URL() {
        return IMAGE_URL;
    }

    public void setIMAGE_URL(String IMAGE_URL) {
        this.IMAGE_URL = IMAGE_URL;
    }
}
